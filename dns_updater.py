#!/usr/local/bin/python3

import boto3
import botocore
from botocore.client import ClientError
import os
import sys
from subprocess import check_output

def get_hosted_zone(domain):
    try:
        client = boto3.client('route53')
        hzs = client.list_hosted_zones()
        # print(hzs['HostedZones'])
        return [hz for hz in hzs['HostedZones'] if domain.endswith(hz['Name'][:-1])][0]
    except Exception as e:
        return None


def update_record_set(zone, domain, ip):
    print('Updating DNS %s...' % domain)
    client = boto3.client('route53')
    response = client.change_resource_record_sets(
        HostedZoneId=zone['Id'],
        ChangeBatch={
            'Changes': [
                {
                    'Action': 'UPSERT',
                    'ResourceRecordSet': {
                        'Name': domain,
                        'Type': 'A',
                        'ResourceRecords': [
                            {
                                'Value': ip
                            }
                        ],
                        'TTL': 300
                    }
                }
            ]
        }
    )
    print(response)


def get_ip():
    return check_output(['wget', 'http://ipinfo.io/ip', '-qO', '-'])[:-1].decode("utf-8") 


def main(args=None):
    my_ip = get_ip()
    # iterate over different domains
    for domain in args:
        zone = get_hosted_zone(domain)

        if zone:
            update_record_set(zone, domain, my_ip)

if __name__ == '__main__':
    main(sys.argv[1:])
